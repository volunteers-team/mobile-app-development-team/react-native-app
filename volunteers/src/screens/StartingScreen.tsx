// @ts-ignore
import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import { Text, Button } from 'react-native-elements';
// @ts-ignore
import logo from '../assets/logo.png';
import { NavigationContainer } from '@react-navigation/native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        fontFamily: "MontserratAlternates-Medium",
        fontWeight: "bold",
        marginBottom: 180,
        fontSize: 36,
    },
    logo: {
        marginBottom: 23,
        width: 92,
        height: 92,
    },
    button_1: {
        marginBottom: 23,
        width: 239,
        height: 48,
        borderRadius: 24,
        backgroundColor: "#000000",
    },
    button_2: {
        marginBottom: 23,
        width: 239,
        height: 48,
        borderRadius: 24,
        backgroundColor: "#000000"
    },
    btn_title: {
        color: "#fff",
        fontFamily: "MontserratAlternates-Medium",
        fontWeight: "bold",
    }
});

function StartingScreen({ navigation }) {
    return (
        <View style={styles.container}>
            <Image
                source={{ uri: logo }}
                style={styles.logo}
            />
            <Text style={styles.label}>Волонтеры</Text>
            <Button
                title="Мне нужна помощь"
                buttonStyle={styles.button_1}
                titleStyle={styles.btn_title}
            />
            <Button
                title="Я - волонтер"
                buttonStyle={styles.button_2}
                titleStyle={styles.btn_title}
                onPress={() => navigation.navigate('LoginScreen')}
            />
        </View>
    );
}

export default StartingScreen;
