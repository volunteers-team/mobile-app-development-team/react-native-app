// @ts-ignore
import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import LoginForm from "../components/LoginForm"
import {Button} from "react-native-elements";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        marginBottom: 23,
        width: 273,
        height: 48,
        borderRadius: 24,
        backgroundColor: "#000000",
    },
    btn_title: {
        color: "#fff",
        fontFamily: "MontserratAlternates-Medium",
        fontWeight: "bold",
        fontSize: 18,
    },
    label: {
        fontFamily: "MontserratAlternates-Medium",
        fontWeight: "bold",
        marginBottom: 60,
        fontSize: 24,
    },
});

function LoginScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.label}>Войдите в систему</Text>
            <LoginForm />
        </View>
    );
}

export default LoginScreen;
