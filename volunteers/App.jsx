import React from 'react';
import { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import StartingScreen from "./src/screens/StartingScreen";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from "./src/screens/LoginScreen";

import * as Font from 'expo-font';
import { AppLoading } from 'expo';

const fetchFonts = () => {
    return Font.loadAsync({
        'MontserratAlternates-Black': require('./src/assets/fonts/MontserratAlternates-Black.ttf'),
        'MontserratAlternates-BlackItalic': require('./src/assets/fonts/MontserratAlternates-BlackItalic.ttf'),
        'MontserratAlternates-Bold.ttf': require('./src/assets/fonts/MontserratAlternates-Bold.ttf'),
        'MontserratAlternates-Medium': require('./src/assets/fonts/MontserratAlternates-Medium.ttf'),
    });
};

const Stack = createStackNavigator();

export default function App() {

  const [dataLoaded, setDataLoaded] = useState(false);

  if (!dataLoaded) {
      return (
        <AppLoading
            startAsync={fetchFonts}
            onFinish={() => setDataLoaded(true)}
        />
      );
  }

  return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="StartingScreen">
          <Stack.Screen
              name="StartingScreen"
              component={StartingScreen}
              options={{headerShown: false}} />
          <Stack.Screen
              name="LoginScreen"
              component={LoginScreen}
              options={{headerShown: false}} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
